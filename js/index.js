document.addEventListener("deviceready", function(){

    LoadAtlx();
    CoreAtlx.requires.addFile({
        type: "js", url: "js/atlx.jquery.js"
    });
    CoreAtlx.loadLast.addFile({
        type: "js", url: "js/tyreapp.js"
    });
    CoreAtlx.init(function(){
        var utils = meadyster.utils;

        if(CoreAtlx.initialized){
            console.log('load complete already called');
            return;
        }
        console.log('load complete called');
        CoreAtlx.ExternalLibs.jQueryLib.runUnder(function(){

            j$('body' ).css('display','inherit');
            j$('#nav-panel' ).css('display','block');
            TyreApp.opAppInitialized();

            if(utils.isDefined(TyreApp.db.loaded) && TyreApp.db.loaded){
                TyreApp.loadingComplete();
                TyreApp.setupGauge();
            }
        });
        CoreAtlx.initialized = true;
    });

});




