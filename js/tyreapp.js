/**
 * Created by AManaloiu on 15/06/2015.
 */


var fg = {};
fg.utils = meadyster.utils;

fg.utils.refreshPage = function refreshPage(){
    j$.mobile.changePage( window.location.href, {
        allowSamePageTransition: true,
        transition: 'none',
        reloadPage: true
    } );
};

fg.utils.valuesToArray = function valuesToArray( fields, from ){
    var arr = [];
    for( var i = 0; i < fields.length; ++i ){
        arr.push( from[fields[i]] );
    }
    return arr;
};

//Tyre object
fg.Tyre = function Tyre(){
    this.manufacturer = 0;
    this.model = 0;
    this.size = 0;
    this.load = 0;
    this.pressure = 0;
    this.modelId = 0;
    this.sizeId = 0;
};

fg.Tyre.prototype.valid = function(){
    return this.manufacturer != "0" &&
        this.model != "0" &&
        this.size != "0" && this.load > 0
};

fg.Tyre.prototype.reset = function(){
    this.manufacturer = 0;
    this.model = 0;
    this.size = 0;
    this.load = 0;
    this.pressure = 0;
    this.modelId = 0;
    this.sizeId = 0;
};

//DataObject
fg.DataObject = function DataObject(){
    this.backvalide = false;
    this.frontvalide = false;
    this.main = {
        machine: "",
        speed: 0
    };
    this.frontTyre = new fg.Tyre();
    this.backTyre = new fg.Tyre();
};

fg.DataObject.prototype.reset = function(){
    this.backvalide = false;
    this.frontvalide = false;
    //this.main.machine = "";
    this.main.speed = 0;
    this.frontTyre.reset();
    this.backTyre.reset();
};

//DatabaseObject
fg.DatabaseManager = function DatabaseManager(){

};

var utils = fg.utils;

//Application main

window.TyreApp = {
    dataObj: new fg.DataObject(),
    placeholder: '<option value="0" disabled selected style=" display: none;">Select</option>',

    scrollToElem: function( elm ){
        j$( 'html,body' ).animate( { scrollTop: j$( elm ).offset().top }, 800 );
        this.scaleContent();
    },

    scaleContent: function(){
        var ui_cont = j$( ".ui-page-active .ui-content" );
        ui_cont.css( "height", "auto" );
        var ch = ui_cont.outerHeight();
        var hh = j$( ".ui-page-active .ui-header" ).outerHeight();
        var fh = j$( ".ui-page-active .ui-footer" ).outerHeight();
        var bd = j$( "body" ).outerHeight();

        var extraSpace = bd - hh - fh - ch;
        if( extraSpace > 0 ){
            ui_cont.height( ch + extraSpace + 20 );
        }
    },

    setupGauge: function(){
        var slider = j$( "#gauge-slider" );

        var settext = function( event, ui ){
            var speedVal = TyreApp.dataObj.main.speed = j$( this ).val();
            var proc = parseInt( speedVal );
            TyreApp.dataObj.main.speedPrc = proc * 290 / 100;
            j$( "#gauge-txt" ).text( speedVal + " Km/h" );

        }

        var animate = function( event, ui ){
            var prc = TyreApp.dataObj.main.speedPrc;
            j$( "#gauge-wr img" ).css( {
                '-webkit-transform':'rotate(' + prc + 'deg)',
                '-moz-transform'   :'rotate(' + prc + 'deg)',
                '-ms-transform'    :'rotate(' + prc + 'deg)',
                '-o-transform'     :'rotate(' + prc + 'deg)',
                'transform'        :'rotate(' + prc + 'deg)'
            } );
        }
        slider.off( 'change', settext );
        slider.off( 'change', animate );
        slider.change(settext);
        slider.change(animate);
    },

    setupScaling: function(){
        var me = this;
        j$( window ).on( "resize orientationchange", function(){
            me.scaleContent();
        } );
    },

    setupEvents: function(){
        var me = this;
        var data = this.dataObj;
        var switchCalcEvt = function( calc ){
            calc = calc ? calc : false;
            var c = j$( "#calculate" );
            if( calc == true ){
                c.off( "touchstart", caclFunc );
                c.on( "touchstart", switchCalcEvt );
                c.text( 'Restart' );

                j$( '#favourites-btn' ).show( 200, function(){
                    me.scrollToElem( '#calculate' );
                } );

            }else{

                j$( '#favourites-btn' ).hide(0);
                me.dataObj.reset();

                j$( ".choice-machine-type" ).each( function( index ){
                    var name = this.id.replace( "select-", "" );
                    j$( this ).css( 'background-image', "url(img/" + name + ".png)" );
                } );
                j$( "#results-area" ).slideUp( 10, function(){
                    c.off( "touchstart", switchCalcEvt );
                    c.on( "touchstart", caclFunc );

                    j$( "#select-choice-man" ).val( "0" ).selectmenu( 'refresh' );
                    j$( "#select-choice-man1" ).val( "0" ).selectmenu( 'refresh' );

                    TyreApp.resetModelList();
                    TyreApp.resetModelList( '1' );
                    j$( "#select-choice-model" ).val( "0" ).selectmenu( 'refresh' );
                    j$( "#select-choice-model1" ).val( "0" ).selectmenu( 'refresh' );

                    TyreApp.resetSizeList();
                    TyreApp.resetSizeList( '1' );
                    j$( "#select-choice-size" ).val( "0" ).selectmenu( 'refresh' );
                    j$( "#select-choice-size1" ).val( "0" ).selectmenu( 'refresh' );

                    j$( "#tyreload" ).val( "" ).textinput( 'refresh' );
                    j$( "#tyreload1" ).val( "" ).textinput( 'refresh' );

                    j$( '.ui-input-clear' ).addClass( 'ui-input-clear-hidden' );
                    c.text( 'Calculate' );

                    j$( "#machine-type" ).collapsible( 'expand' );
                } );
            }
        };
        //calculate click/tap event
        me.caclFunc = function( event, ui ){
            me.collect();
            if( data.main.machine && (data.frontTyre.valid() || data.backTyre.valid()) ){
                j$( "#selection-set" ).children().hide(0);

                me.checkSpeed( "#select-choice-model", function(){
                    me.checkSpeed( "#select-choice-model1", function(){
                        me.fillResults();
                        j$( '#favourites-btn' ).show();
                        me.scrollToElem('#form-page-header');
                    });
                });
            }
            else{
                if( !data.main.machine ){
                    me.machineTypeNotSelected( event );
                }
                else if( !data.frontvalide && !data.backvalide ){
                    utils.endEvent( event );
                    switchPage(3);
                    navigator.notification.alert( "Please fill in some data for the tyres !", null, 'Attention', 'Ok');
                }
            }
        };
        j$( ".calculate" ).on( "touchstart", me.caclFunc );
        //machine icon select event
        j$( ".choice-machine-type" ).on( "vclick", function( ev, ui ){
            j$( ".choice-machine-type" ).each( function( index ){
                var name = this.id.replace( "select-", "" );
                j$( this ).css( 'background-image', "url(img/" + name + ".png)" );
            } );
            data.main.machine = this.id.replace( "select-", "" );
            if( data.main.machine == 'trailed' ){
                j$( '#next-back' ).hide( 100, function(){
                    j$( '#ftyre .ui-collapsible-heading .collapsible-header-title' ).text( 'Tyres' );
                } );
            }
            else{
                j$( '#next-back' ).show( 100, function(){
                    j$( '#ftyre .ui-collapsible-heading .collapsible-header-title' ).text( 'Front Tyres' );
                } );
            }
            j$( this ).css( 'background-image', "url(img/" + data.main.machine + "_selected.png)" );
            j$( '#machine-type' ).hide();
            j$( '#machine-speed' ).collapsible( 'expand' ).show();
        } );

        j$( "#fav-list" ).on( "touchstart", "li", function(){
            me.loadFavourite( j$( this ) );
        } );
        j$( document ).on( "pagebeforeshow", "#favourite-page", function(){
            me.db.getFav();
        } );
        j$( document ).on( "swiperight", "#fav-list > li", function( event ){
            utils.endEvent( event );

            j$( this ).hide( "slide", { direction: "right" }, 500, function(){
                var litem = j$( this );
                me.db.remFav( me.getFavouriteNameFromLI( litem ) );
                litem.remove();
            } );

            j$( '#fav-list' ).listview( 'refresh', true ).enhanceWithin();
        } );

        //j$( document ).on( "swipeleft swiperight", ".content", function( event ) {
        //var panel = j$( "#nav-panel" );
        //if(event.type === 'swipeleft'){
        //panel.panel('close');
        //}
        //else if(event.type === 'swiperight'){
        //panel.panel('open');
        //}
        //});

        j$( '#select-choice-man' ).on( "change", function( event ){
            if( !data.main.machine ){
                return me.machineTypeNotSelected( event );
            }
            var val = j$( this ).val();
            me.db.populateModel( data.main.machine, parseInt( val ), '' );
        } );
        j$( '#select-choice-man1' ).on( "change", function( event ){
            if( !data.main.machine ){
                return me.machineTypeNotSelected( event );
            }
            var val = j$( this ).val();
            me.db.populateModel( data.main.machine, parseInt( val ), 1 );
        } );
        j$( '#select-choice-model' ).on( "change", function(){
            me.db.populateSize( j$( this ).find( 'option:selected' ).text(), '' );
        } );
        j$( '#select-choice-model1' ).on( "change", function(){
            me.db.populateSize( j$( this ).find( 'option:selected' ).text(), 1 );
        } );
        j$( '#select-choice-size, #tyreload' ).on( "change", function(){

            me.checkSpeed( "#select-choice-model", function(){
                if( me.frontSectionValid() ){

                }
            } );

        } );
        j$( '#select-choice-size1, #tyreload1' ).on( "change", function(){

            me.checkSpeed( "#select-choice-model1", function(){
                if( me.backSectionValid() ){

                }
            } );
        } );
        j$( "#tyreload1, #tyreload" ).focusout( function( evt ){
            var v = parseInt( this.value );
            v = isNaN( v ) ? 0 : v;
            v += ' Kg';
            j$( this ).val( v ).attr( 'value', v ).textinput( 'refresh' );

            if( this.id == "tyreload" ){
                me.db.checkValidSpeedLoad( function( tx, results ){
                    console.log( 'TODO checkValidSpeedLoad' );
                    //var len = results.rows.length;
                    //var item = results.rows.item(i);
                    //console.log('Front Tyre:speed=>'+ data.speed + ' load=>' + data.load);
                } );
            }
            else if( this.id == "tyreload1" ){
                me.db.checkValidSpeedLoad( function( tx, results ){
                    console.log( 'TODO checkValidSpeedLoad' );
                    //var len = results.rows.length;
                    //var item = results.rows.item(i);
                    //console.log('Front Tyre:speed=>'+ data.speed + ' load=>' + data.load);
                } );
            }
        } );
        j$( '.ui-icon-star' ).on( 'touchstart', function(){
            me.addToFavourite();
        } );
        j$( '#favourite-page' ).on( "pagecreate", function( event, ui ){
            me.db.getFav();
        } );

        this.pageHistoryCount = 0;
        this.goingBack = false;

        j$( document ).on( "pagecontainershow", function( e, data ){
            if( me.goingBack ){
                me.goingBack = false;
            }else{
                me.pageHistoryCount++;
            }
            console.log( "pagecontainershow " + me.pageHistoryCount );

            //var id = data.toPage.get( 0 );
            //if(data.toPage.get(0).id == 'form-page'){
                //me.resetCalculation(false);
            //}

        } );
        j$( document ).on( "pagecontainerbeforeshow", function( e, data ){

            if( data.toPage.get( 0 ).id == 'form-page' ){
                me.setupGauge();
                j$( "#results-area, #btyre, #ftyre, #machine-speed" ).hide( 0 );
                j$( "#machine-type" ).collapsible( 'expand' ).show( 0 );
            }
        });

        document.addEventListener( "backbutton", function( e ){
            e.preventDefault();
            if( me.pageHistoryCount > 0 ){
                me.pageHistoryCount--;
            }
            console.log( "backbutton " + me.pageHistoryCount );
            //if( me.pageHistoryCount == 0 ){
                navigator.notification.confirm( "Are you sure you want to quit?", function( result ){
                    if( result == 2 ){
                        navigator.app.exitApp();
                    }else{
                        me.pageHistoryCount++;
                    }
                }, 'Quit TyreApp', 'Cancel,Ok' );
            //}else{
                ///goingBack = true;
                //console.log( "Going back to page #" + me.pageHistoryCount );
                //navigator.app.backHistory();
            //}
        }, false );

        var switchPage = function(page){

            j$( '#machine-type, #machine-speed, #ftyre, #btyre, #results-area' ).hide( 0 );
            switch (page)
            {
                case 1:
                    j$( '#favourites-btn' ).hide(0);
                    me.resetForm();
                    j$( '#machine-type' ).collapsible( 'expand' ).show( 0 );
                    break;
                case 2:
                    j$( '#machine-speed' ).collapsible( 'expand' ).show(0);
                    break;
                case 3:
                    j$( '#ftyre' ).collapsible( 'expand' ).show(0);
                    break;
                case 4:
                    j$( '#btyre' ).collapsible( 'expand' ).show(0);
                    break;
                case 5:
                    break;
            }

            me.scrollToElem( '#form-page-header' );
        };

        j$( '#open-calculator, #back-machine' ).on( 'touchstart', function( event ){
            utils.endEvent( event );
            switchPage( 1 );
            me.resetCalculation(true);
        } );

        j$( '#next-speed, #back-speed' ).on( 'touchstart', function( event ){
            utils.endEvent( event );
            switchPage( 2 );

        } );

        j$( '#next-front, #back-front' ).on( 'touchstart', function( event ){
            utils.endEvent( event );
            switchPage( 3 );

        } );

        j$( '#next-back, #back-back' ).on( 'touchstart', function( event ){
            utils.endEvent( event );
            switchPage( 4 );
        } );

    },

    resetCalculation: function(redir){
        this.resetForm();
        if(redir)
        {
            utils.redirect( '#blank' );
            utils.redirect( '#form-page' );
        }
    },

    setup: function(){
        this.setupScaling();
        j$( '#form-page' ).on( 'pageinit', function(){
            TyreApp.setupEvents();
        } );
        this.scaleContent();
        j$( "#result-back" ).slideUp( "slow" );
        j$( "#result-front" ).slideUp( "slow" );
    },

    opAppInitialized: function(){
        //j$( ".choice-machine-type" )
            //.css( 'margin', 'auto' )
            //.css( 'margin-bottom', '15px' );
        this.scaleContent();
        j$( "body>[data-role='panel']" ).panel().enhanceWithin();
        this.setup();
    },

    initDB: function(){
        console.log( 'initDB called' );
        var me = this;
        this.fav = [];

        var dbo = this.db = window.openDatabase( "tyre_database15", "", "Data for Tyres", 2 * 1024 * 1024, function(){
            console.log( 'Db created.' );
            var r = CoreAtlx.requires;
            r.addFile( {
                type: "js",
                url: "js/data.js"
            } );
            r.load = true;
            r.alldone = false;
            r.runUnder( function(){
                me.loadDb();
                localStorage.setItem( "firstrun", "1" );
            } );
            r.loadNow();
        } );
        TyreApp.db.loaded = window.localStorage.getItem( "firstrun" );

        dbo.tables = 0;
        dbo.tables_filled = 0;

        // do something based on a specific table existence
        dbo.ifTableExists = function( table, itis, itsnot ){
            this.transaction( function( tx ){
                tx.executeSql( 'SELECT count(*) FROM sqlite_master WHERE type="table" AND name="' + table + '";', [],
                    function( tx, result ){
                        if( result.rows.item( 0 )['count(*)'] == 0 ){
                            itsnot && itsnot( tx, result );
                        }
                        else{
                            itis && itis( tx, result );
                        }
                    } );
            } );
        };

        dbo.errorCB = function( err ){
            console.log( 'error ' + err.code );
        };

        dbo.successCB = function( tx, results ){
            console.log( 'success' );
        };

        dbo.selectAll = function( tx ){
            tx.executeSql( 'SELECT * FROM tyres', [],
                me.db.successSelectAll,
                me.db.errorCB );

            console.log( 'Db selection done.' );
        };

        dbo.successSelectAll = function( tx, results ){
            console.log( 'results ' + results.rows.length );
        };

        dbo.maxSpeed = function( tyre, callback ){
            dbo.transaction( function( tx ){
                tx.executeSql( 'SELECT\
            tyre_data.`load`,\
            Max(speeds.speed) AS speed,\
                preassures.bar,\
                preassures.psi\
            FROM\
            tyre_data INNER JOIN \
            tyres ON tyres.id = tyre_data.tyre INNER JOIN \
            manufacturers ON manufacturers.id = tyres.manufacturer INNER JOIN \
            machines ON machines.id = tyre_data.machine INNER JOIN \
            speeds ON speeds.id = tyre_data.speed INNER JOIN \
            preassures ON preassures.id = tyre_data.pressure\
            WHERE \
            tyre_data.tyre = ?', [tyre], function( tx, results ){
                    var len = results.rows.length;
                    if( len > 0 ){
                        callback( results.rows.item( 0 ).speed );
                    }
                }, dbo.errorCB );
            } );
        };

        dbo.machineId = function( machine ){

            switch( machine ){
                case 'combines':
                    return 1;
                case 'trailed':
                    return 2;
                case 'tractors':
                    return 3;
                case 'sprayers':
                    return 4;
                case 'any':
                    return 5;
                default :
                    return 0;
            }
        };

        dbo.populateManufacturers = function( callback ){
            dbo.transaction( function( tx ){
                tx.executeSql( 'SELECT * FROM manufacturers', [],
                    function( tx, results ){

                        var len = results.rows.length;
                        var option = '';
                        me.resetManufacturerList();
                        me.resetManufacturerList( 1 );

                        for( var i = 0; i < len; ++i ){
                            var item = results.rows.item( i );
                            option += "<option value=\"" + item.id + "\">" + item.manufacturer.toLocaleUpperCase() + "</option>";
                        }
                        j$( '#select-choice-man' ).append( option );
                        j$( '#select-choice-man' ).selectmenu().selectmenu( 'refresh', true );

                        j$( '#select-choice-man1' ).append( option );
                        j$( '#select-choice-man1' ).selectmenu().selectmenu( 'refresh', true );

                        callback && callback();
                    }, dbo.errorCB );
                console.log( 'Manufacturer select lists populated.' );
            } );
        };

        dbo.populateModel = function( machine, manuId, field, callback ){
            dbo.transaction( function( tx ){

                me.resetModelList( field );

                var query = 'SELECT DISTINCT id, model FROM tyres WHERE manufacturer = "' + manuId + '" AND (machine="' + dbo.machineId( machine ) + '" OR machine="5")';
                tx.executeSql( query, [],
                    function( tx, results ){
                        var len = results.rows.length;
                        var option = '';
                        var model = '';
                        for( var i = 0; i < len; ++i ){
                            var item = results.rows.item( i );
                            if( model != item.model ){
                                option += "<option value=\"" + item.id + "\">" + item.model + "</option>";
                                model = item.model;
                            }
                        }
                        j$( '#select-choice-model' + field ).append( option ).selectmenu( 'refresh', true );
                        callback && callback();
                    }, dbo.errorCB );
                console.log( 'Model select ' + field + 'lists populated.' );
            } );
        };

        dbo.populateSize = function( model, field, callback ){
            dbo.transaction( function( tx ){
                me.resetSizeList( field );

                var query = 'SELECT id, size FROM tyres WHERE model = "' + model + '"';
                tx.executeSql( query, [],
                    function( tx, results ){
                        var len = results.rows.length;
                        var option = '';
                        var size = '';
                        for( var i = 0; i < len; ++i ){
                            var item = results.rows.item( i );
                            if( size != item.size ){
                                option += "<option value=\"" + item.id + "\">" + item.size + "</option>";
                                size = item.size;
                            }
                        }
                        j$( '#select-choice-size' + field ).append( option ).selectmenu( 'refresh', true );

                        callback && callback();
                    }, dbo.errorCB );
                console.log( 'Tyre Size ' + field + 'select lists populated.' );
            } );
        };

        dbo.saveFav = function( name, data ){
            dbo.transaction( function( tx ){
                tx.executeSql( 'INSERT INTO favourites (name, data) VALUES (?,?)', [name, data], function( tx, results ){
                    navigator.notification.alert( name + ' saved to favourites!', null, 'Confirmed' );
                }, dbo.errorCB );
            } );
        };

        dbo.remFav = function( name ){
            dbo.transaction( function( tx ){
                tx.executeSql( 'DELETE FROM favourites WHERE name=?', [name], function( tx, results ){
                    navigator.notification.alert( name + ' removed from favourites!', null, 'Attention' );
                }, dbo.errorCB );
            } );
        };

        dbo.getFav = function(){
            dbo.transaction( function( tx ){
                var favlist = j$( '#fav-list' ).empty();
                tx.executeSql( 'SELECT * FROM favourites', [],
                    function( tx, results ){
                        var len = results.rows.length;
                        var liitems = '';
                        for( var i = 0; i < len; ++i ){
                            var item = results.rows.item( i );
                            me.fav[item.name] = JSON.parse( item.data );
                            liitems += '<li data-corners=\"false\" data-shadow=\"false\" data-iconshadow=\"true\" data-wrapperels=\"div\" data-iconpos=\"right\" data-theme=\"c\" data-icon="carat-r"><a href="#">' + item.name +
                                '</a></li>';
                        }
                        favlist.append( liitems );
                        favlist.listview( 'refresh' ).enhanceWithin();
                    }, dbo.errorCB );
            } );
        };

        dbo.checkValidSpeedLoad = function( callback ){
            dbo.transaction( function( tx ){
                tx.executeSql( 'SELECT * FROM favourites', [], callback, dbo.errorCB );
            } );
        };
    },

    loadDb: function(){
        var me = this;
        this.db.tables = this.tyre_data.length;
        this.db.tables_filled = 0;

        this.tyre_data.forEach( function( table, index, arr ){

            //TyreApp.db.transaction(function (tx){
            //    tx.executeSql('DROP TABLE IF EXISTS '+table.name);
            //});

            me.db.ifTableExists( table.name,
                function( tx, result ){
                    console.log( 'table ' + table.name + ' exists' );
                    me.db.tables_filled++;
                    me.db.transaction( function( tx ){
                        tx.executeSql( 'SELECT * FROM ' + table.name, [],
                            me.db.successSelectAll,
                            me.db.errorCB );
                        console.log( 'Db selection done.' );
                    } );
                },
                function( tx, result ){
                    console.log( 'table ' + table.name + ' doesnt exist' );
                    me.db.transaction( function( tx ){
                            var query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + table.fields.join() + ')';
                            tx.executeSql( query, [],
                                function( tx, result ){
                                    console.log( 'Table ' + table.name + ' created.' );
                                    console.log( 'Populating table ' + table.name + '.' );

                                    table.records.forEach( function( row, index, arr ){
                                        var sql = 'INSERT INTO ' + table.name + '(' + table.fields.join() + ') VALUES ("' + utils.valuesToArray( table.fields,
                                                row ).join( "\", \"" ) + '")';
                                        tx.executeSql( sql );
                                    } );
                                    //Insert data..................

                                    console.log( 'Table ' + table.name + ' populated.' );
                                } );

                            console.log( 'Db populated.' );
                        },
                        me.db.errorCB,
                        function( tx, results ){
                            var DB = me.db;
                            DB.tables_filled++;

                            if( DB.tables == DB.tables_filled ){
                                DB.loaded = true;
                                window.localStorage.setItem( "firstrun", true);
                                me.loadingComplete();
                            }
                        } );
                } );
        } );

        this.db.ifTableExists( 'favourites',
            function( tx, result ){},
            function( tx, result ){
                console.log( 'table favourites doesnt exist' );
                me.db.transaction( function( tx ){
                    var query = 'CREATE TABLE IF NOT EXISTS favourites (name TEXT NOT NULL, data TEXT NOT NULL )';
                    tx.executeSql( query, [],
                        function( tx, result ){
                            console.log( 'Table favourites created.' );
                        } );
                }, me.db.errorCB );
            } );
        console.log( 'database populated successfully ' );
    },

    loadingComplete: function(){
        var l = j$( '#login-wig' );
        if( l.length){

            l.css( 'visibility', 'visible' );
            TyreApp.setupLogin();

        }else{

            this.db.populateManufacturers();
            utils.redirect( '#form-page' );
        }
    },

    machineTypeNotSelected: function( event ){
        j$( "#ftyre" ).hide();
        j$( "#btyre" ).hide();
        j$( "#machine-type" ).collapsible( 'expand' ).show();
        navigator.notification.alert( 'Please select machine type first !', null, 'Attention' );
        utils.endEvent( event );
    },

    resetManufacturerList: function( field ){
        field = field ? field : '';
        j$( '#select-choice-man' + field )
            .empty()
            .append( this.placeholder ).selectmenu()
            .selectmenu( 'refresh', true );
        this.resetModelList( field );
    },

    resetModelList: function( field ){
        field = field ? field : '';
        j$( '#select-choice-model' + field )
            .empty()
            .append( this.placeholder ).selectmenu()
            .selectmenu( 'refresh', true );
        this.resetSizeList( field );
    },

    resetSizeList: function( field ){
        field = field ? field : '';
        j$( '#select-choice-size' + field )
            .empty()
            .append( this.placeholder ).selectmenu()
            .selectmenu( 'refresh', true );
    },

    collect: function(){
        var data = this.dataObj;
        data.reset();

        data.main.speed = j$( "#gauge-slider" ).val();

        data.frontTyre.manufacturer = j$( "#select-choice-man" ).val();
        data.frontTyre.model = j$( "#select-choice-model option:selected" ).text();
        data.frontTyre.modelId = j$( "#select-choice-model" ).val();
        data.frontTyre.size = j$( "#select-choice-size option:selected" ).text();
        data.frontTyre.sizeId = j$( "#select-choice-size" ).val();
        data.frontTyre.load = parseInt( j$( "#tyreload" ).val() );

        data.backTyre.manufacturer = j$( "#select-choice-man1" ).val();
        data.backTyre.model = j$( "#select-choice-model1 option:selected" ).text();
        data.backTyre.modelId = j$( "#select-choice-model1" ).val();
        data.backTyre.size = j$( "#select-choice-size1 option:selected" ).text();
        data.backTyre.sizeId = j$( "#select-choice-size1" ).val();
        data.backTyre.load = parseInt( j$( "#tyreload1" ).val() );

        return data;
    },

    resetForm: function(){
        this.dataObj.reset();
        this.dataObj.main.machine = null;

        j$( ".choice-machine-type" ).each( function( index ){
            var name = this.id.replace( "select-", "" );
            j$( this ).css( 'background-image', "url(img/" + name + ".png)" );
        } );

        j$( "#gauge-txt" ).text( 0 + " Km/h" );
        j$( "#gauge-wr img" ).css( {
            '-webkit-transform': 'rotate(' + 0 + 'deg)',
            '-moz-transform': 'rotate(' + 0 + 'deg)',
            '-ms-transform': 'rotate(' + 0 + 'deg)',
            '-o-transform': 'rotate(' + 0 + 'deg)',
            'transform': 'rotate(' + 0 + 'deg)'
        } );
        j$( "#gauge-slider" ).val( 0 );
        j$( "#gauge-slider" ).slider( 'refresh' );

        this.resetModelList();
        this.resetModelList( 1 );

        j$( "#select-choice-man" ).val( "0" ).selectmenu( 'refresh' );
        j$( "#select-choice-man1" ).val( "0" ).selectmenu( 'refresh' );

        j$( '#ftyre .ui-collapsible-heading .collapsible-header-title' ).text( 'Front Tyres' );

        j$( "#tyreload" ).val( '' ).textinput( 'refresh' );
        j$( "#tyreload1" ).val( '' ).textinput( 'refresh' );
    },

    frontSectionValid: function(){
        return this.collect().frontTyre.valid();
    },

    backSectionValid: function(){
        return this.collect().backTyre.valid();
    },

    frontBackValid: function(){
        if( this.frontvalide && this.backvalide ){
            return 1;
        }
        else if( this.frontvalide ){
            return 2;
        }
        else if( this.backvalide ){
            return 3;
        }
    },

    setResultPressure: function( result_field, tid, load, spd ){
        var me = this;
        this.db.transaction( function( tx ){
            tx.executeSql( 'SELECT\
            Min(results.mload),\
                results.speed,\
                results.bar,\
                results.psi,\
                results.tyre\
            FROM\
            (SELECT\
            tyre_data.`load` AS mload,\
                speeds.speed,\
                preassures.bar,\
                preassures.psi,\
                tyre_data.tyre\
            FROM\
            tyre_data INNER JOIN\
            tyres ON tyres.id = tyre_data.tyre INNER JOIN\
            manufacturers ON manufacturers.id = tyres.manufacturer INNER JOIN\
            machines ON machines.id = tyre_data.machine INNER JOIN\
            speeds ON speeds.id = tyre_data.speed INNER JOIN\
            preassures ON preassures.id = tyre_data.pressure\
            WHERE\
            tyre_data.tyre = ? AND\
            CAST(speeds.speed AS INTEGER) >= ? AND\
            CAST(tyre_data.`load` AS INTEGER) >= ?) results', [tid, spd, load],
                function( tx, results ){
                    var len = results.rows.length, item;
                    if( len > 1 ){

                    }
                    else if( len == 0 ){
                        j$( result_field )
                            .text( '0 psi / 0 bar' );
                    }
                    else{
                        item = results.rows.item( 0 );
                        if( item['results.psi'] != null)
                        {
                            j$( result_field )
                                .text( item['results.psi'] + ' psi / ' + item['results.bar'] + ' bar' );
                        }
                        else{
                            j$( result_field )
                                .text( 'Load to high !' );
                        }

                    }
                }, me.db.errorCB );
        } );
    },

    getFavouriteNameFromLI: function( li ){
        return li.find( 'a' ).text().trim();
    },

    addToFavourite: function(){
        var me = this;
        var n = '';
        var data = {
            name: '',
            main: me.dataObj.main,
            front: me.dataObj.frontTyre,
            back: me.dataObj.backTyre
        };
        var getFavName = function(){
            navigator.notification.prompt( 'Please name this favourite',function(results){
                if(results.buttonIndex == 1){
                    if(data.name.length >= 0){
                        n = data.name = results.input1;
                        navigator.notification.confirm(
                            'You are about to save this as ' + data.name + '. Do you want to proceed?',
                            function(index){
                                if(index == 1){
                                    data = JSON.stringify( data );
                                    me.db.saveFav( n, data );
                                }
                            },
                            'Save to favourite',
                            'OK,CANCEL'
                        );
                    }
                    else{
                        navigator.notification.alert('Please provide a name for your favourite',function(){
                            getFavName();
                        }, 'Attention');
                    }
                }
            }, 'Save favourite',['Ok','Cancel'], 'favourite' );
        };
        getFavName();
    },

    fillResultDetails:function(f){
        var list = j$(f).parent().parent().find('.details > div');
        var tyre = null;

        if(f == '#result-front'){
            tyre = this.dataObj.frontTyre;
        }
        else{
            tyre = this.dataObj.backTyre;
        }

        tyre.model && j$(list[0]).html( "<strong>Model:</strong> " + tyre.model + " " + tyre.size);
        tyre.load && j$(list[1]).html( "<strong>Load:</strong> " + tyre.load + " Kg");
        this.dataObj.main.speed && j$(list[2]).html( "<strong>Speed:</strong> " + this.dataObj.main.speed + " Km/h");
    },

    fillResults: function(){
        var me = this;
        var data = this.dataObj;
        var noInput = 'No input data!';
        if( data.frontTyre.valid() ){
            this.frontvalide = true;
            this.setResultPressure( "#result-front", data.frontTyre.sizeId, data.frontTyre.load, data.main.speed );
            this.fillResultDetails( "#result-front");
        }
        else{
            j$( "#result-front" ).text( noInput );
        }
        if( data.main.machine == 'trailed' ){
            j$( '#results-area .ui-listview .ui-last-child' ).hide( 0, function(){
                j$( '#results-area .ui-listview .ui-first-child .result-label' ).text( 'Tyre' );
            } );
        }
        else{
            j$( '#results-area .ui-listview .ui-last-child' ).show( 0, function(){
                j$( '#results-area .ui-listview .ui-first-child .result-label' ).text( 'Front' );
            } );
            if( data.backTyre.valid() ){
                me.backvalide = true;
                this.setResultPressure( "#result-back", data.backTyre.sizeId, data.backTyre.load, data.main.speed );
                this.fillResultDetails( "#result-back" );
            }
            else{
                j$( "#result-back" ).text( noInput );
            }
        }
        j$( "#results-area" ).show(0);
        j$( "#result-front" ).show(0);
        j$( "#result-back" ).show(0);
    },

    setSelections: function( manu, model, size, data ){
        manu.find( 'option[value=0]' ).removeAttr( 'selected' );
        manu.find( 'option[value=' + data.manufacturer + ']' ).attr( 'selected', 'selected' );
        manu.selectmenu( 'refresh', true );

        model.find( 'option[value=0]' ).removeAttr( 'selected' );
        model.find( 'option[value=' + data.modelId + ']' ).attr( 'selected', 'selected' );
        model.selectmenu( 'refresh', true );

        size.find( 'option[value=0]' ).removeAttr( 'selected' );
        size.find( 'option[value=' + data.sizeId + ']' ).attr( 'selected', 'selected' );
        size.selectmenu( 'refresh', true );
    },

    loadFavourite: function( li ){
        var me = this;
        var favname = this.getFavouriteNameFromLI( li );
        var favData = this.fav[favname];
        var prc = favData.main.speedPrc;
        utils.redirect( '#form-page' );

        this.db.populateModel( favData.main.machine, favData.front.manufacturer, '', function(){
            me.db.populateSize( favData.front.model, '', function(){
                var scman = j$( '#select-choice-man' );
                var scmod = j$( '#select-choice-model' );
                var scsi = j$( '#select-choice-size' );

                me.setSelections( scman, scmod, scsi, favData.front );
                j$( "#tyreload" ).val( favData.front.load );
                me.db.populateModel( favData.main.machine, favData.back.manufacturer, 1, function(){
                    me.db.populateSize( favData.back.model, 1, function(){
                        scman = j$( '#select-choice-man1' );
                        scmod = j$( '#select-choice-model1' );
                        scsi = j$( '#select-choice-size1' );

                        me.setSelections( scman, scmod, scsi, favData.back );
                        j$( "#tyreload1" ).val( favData.back.load );
                        me.caclFunc();

                        me.fillResultDetails( "#result-front");
                        me.fillResultDetails( "#result-back");
                    } );
                } );
            } );
        } );

        this.dataObj.main.machine = favData.main.machine;
        j$( ".choice-machine-type" ).each( function( index ){
            var name = this.id.replace( "select-", "" );
            if( name == favData.main.machine ){
                j$( this ).css( 'background-image', "url(img/" + favData.main.machine + "_selected.png)" );
            }
            else{
                j$( this ).css( 'background-image', "url(img/" + name + ".png)" );
            }
        } );

        if( favData.main.machine == 'trailed' ){
            j$( '#btyre' ).hide( 100, function(){
                j$( '#ftyre .ui-collapsible-heading .collapsible-header-title' ).text( 'Tyres' );
            } );
        }
        else{
            j$( '#btyre' ).show( 100, function(){
                j$( '#ftyre .ui-collapsible-heading .collapsible-header-title' ).text( 'Front Tyres' );
            });
        }

        j$( "#gauge-txt" ).text( favData.main.speed + " Km/h" );
        j$( "#gauge-wr img" ).css( {
            '-webkit-transform': 'rotate(' + prc + 'deg)',
            '-moz-transform': 'rotate(' + prc + 'deg)',
            '-ms-transform': 'rotate(' + prc + 'deg)',
            '-o-transform': 'rotate(' + prc + 'deg)',
            'transform': 'rotate(' + prc + 'deg)'
        } );
        j$( "#gauge-slider" ).val( favData.main.speed );
        j$( "#gauge-slider" ).slider( 'refresh' );
        j$( "#machine-type" ).collapsible( 'expand' );
    },

    checkSpeed: function( model, valid ){

        var me = this;
        var tyre = j$( model ).val();
        var sspeed = this.dataObj.main.speed;
        this.db.maxSpeed( tyre, function( speed ){

            if( parseInt( sspeed ) > parseInt( speed ) ){
                navigator.notification.alert( 'Maximum speed for the selected tyre is ' + speed, null , 'Attention' );
                j$( "#ftyre" ).hide();
                j$( "#btyre" ).hide();
                j$( "#machine-speed" ).collapsible( 'expand' ).show();
            }
            else{
                valid && valid();
            }
        } );
    },
    haveNetwork:function(){
        return navigator.connection.type != Connection.UNKNOWN && navigator.connection.type != Connection.NONE;
    },
    networkError:function(){
        navigator.notification.alert( 'Network Error please try again later !', null, 'Attention');
    },
    setupLogin:function(){

        var need_login = false;
        var have_network = this.haveNetwork();
        var lastLogin = window.localStorage.getItem( "last-login" );

        console.log('have_net ' + have_network + '  ' + navigator.connection.type);

        if(lastLogin){
            if(!have_network){
                utils.redirect('app.html');
                return;
            }
            console.log(lastLogin);
            lastLogin = new Date(lastLogin);
            var dps = utils.daysPassed(lastLogin);
            console.log(dps);

            if(dps > 14){
                window.localStorage.removeItem("last-login");
                need_login = true;
            }
            else{
                utils.redirect('app.html');
                return;
            }
        }
        else{
            need_login = true;
        }
        var me = this;
        need_login && j$('#login-wig form').on('submit', function(evt){
            utils.endEvent( event );
            have_network = me.haveNetwork();
            var timestamp = new Date().toISOString();
            var em = j$('#user-email' ).val();
            var ps = j$('#user-password' ).val();
            var d = {
                "Email":em,
                "Password":ps
            };
            var ds = JSON.stringify(d);

            var md = md5(ds);

            var shaObj = new jsSHA("SHA-512", "TEXT");
            shaObj.update("POST+/rest/BMStaging/login+"+md+"+2exe4ku71SlQl7ptg3Me6yq4qKfN51+"+timestamp);
            var signature = shaObj.getHash("HEX");

            console.log('email+pass: ' + ds);
            console.log('md5: ' + md);
            console.log('sha: ' + signature);
            console.log('timestamp: ' + timestamp);

            if(have_network){

                $.ajax({
                    url: "https://fgstaging.affino.com/rest/BMStaging/login",
                    headers: {
                        'Authorization':"Basic ZmdhZG1pbjpnXjlEPGchQQ==",
                        'Content-Type':"application/json; charset=UTF-8",
                        'MD5':md,
                        'Signature':signature,
                        'TimeStamp':timestamp,
                        'APIKey':'zzQ81g0EDi'
                    },
                    method: 'POST',
                    dataType: 'json',
                    data: ds,
                    success: function(data, status, jqXHR){
                        if(data.AccessToken && data.RefreshToken && parseInt(data.Expires) > 0){
                            window.localStorage.setItem( "last-login",(new Date).toUTCString() );
                            utils.redirect('app.html');
                        }
                    },
                    complete:function(jqXHR, textStatus){
                        console.log(jqXHR.responseText);
                        console.log(textStatus);
                    },
                    error:function(jqXHR, textStatus, errorThrown){
                        console.log(jqXHR.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);

                        var obj = {error:null};
                        if(jqXHR.responseText){
                            obj = jQuery.parseJSON( jqXHR.responseText );
                        }
                        if(obj.error){
                            if(obj.error.code)
                            {
                                if(obj.error.code == -111){
                                    navigator.notification.confirm('Your login details are wrong. If you have forgotten your username or password please visit FGInsight website by clicking the button below.',
                                        function(btn){
                                            if(btn == 1){
                                                navigator.app.exitApp();
                                            }else if(btn == 2){
                                                window.open("https://www.fginsight.com/passwordreminder", '_system');
                                            }
                                        }, 'Attention', 'Cancel, FGInsight');
                                }else if(obj.error.code == -112){
                                    navigator.notification.confirm('You do not have the right subscription level to use this app. To upgrade please visit FGInsight website by clicking the button below.',
                                        function(btn){
                                            if(btn == 1){
                                                navigator.app.exitApp();
                                            }else if(btn == 2){
                                                window.open("https://www.fginsight.com/subscriptions", '_system');
                                            }
                                        }, 'Attention', 'Cancel, FGInsight');
                                }
                                else{
                                    me.networkError();
                                }
                            }
                            else{
                                me.networkError();
                            }
                        }
                        else{
                            me.networkError();
                        }
                    }
                });

            }
            else{
                navigator.notification.confirm('No internet connection! Please connect to internet and try again',
                    function(btn){
                    if(btn == 'Exit'){
                        navigator.app.exitApp();
                    }else{
                        utils.redirect('index.html');
                    }
                }, 'Attention', 'Exit, TryAgain')
            }

        });
    }

};
TyreApp.initDB();
console.log( 'tyreapp loaded' );
