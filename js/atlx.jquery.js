/**
 * Created by AManaloiu on 15/06/2015.
 */
var utils = meadyster.utils;
var conf = meadyster.conf;
var CoreAtlx = meadyster.Atlx;

if ((utils.isDefined(conf.jQuery)) || (utils.isDefined(conf.loadjQuery) && conf.loadjQuery)) {

    utils.defineOnce('jQueryLib', CoreAtlx.ExternalLibs, function () {
        jQueryLib = new CoreAtlx.CoreClasses.LibObject('jQueryLib', 1, conf.loadjQuery);
        jQueryLib.que = [];
        jQueryLib.isLoaded = function () {
            return utils.isDefined(window.jQuery);
        }
        // wrapper for the document ready event
        jQueryLib.runUnder = function (func) {
            if (this.isLoaded()) {
                console.log('running under jquery');
                j$(document).ready(func);
            }
            else {
                this.que.push(func);
                console.log('jQuery not loaded');
            }
        };
        jQueryLib._onLoaded = function () {
            window.j$ = jQuery;
            console.log('jQuery Loaded as Atlx.ExternalLibs.jQueryLib ...');
            CoreAtlx.CoreClasses.LibObject.prototype._onLoaded.apply(this);
        };
        jQueryLib.runUnder(function () {
            //definitions using jQuery
            //called as soon as jQuery is available
            console.log('jquery doc ready called');
            j$.support.cors = true;
            j$.mobile.allowCrossDomainPages = true;
            utils.runIfField = j$.runIfField = function runIfField(field, fn) {
                var fld = j$(field);
                if (fld.length > 0) {
                    fn(fld);
                }
            };
            console.log('runIfField defined');
        });
        CoreAtlx.addExternal(jQueryLib);

        if (!utils.isDefined(conf.jQuery)) {
            var minified = '.min';
            if(conf.debug){
                minified = '';
            }
            // add jquery related files to load in the right order
            //jQueryLib.addFile({type: "css", url: "css/jquery-ui"+minified+".css"});
            //jQueryLib.addFile({type: "css", url: "css/jquery-ui.structure"+minified+".css"});
            //jQueryLib.addFile({type: "css", url: "css/jquery-ui.theme"+minified+".css"});
            //jQueryLib.addFile({type: "css", url: "css/jquery.mobile.custom.theme"+minified+".css"});
            //jQueryLib.addFile({type: "css", url: "css/jquery.mobile.custom.structure"+minified+".css"});
            //jQueryLib.addFile({type: "css", url: "css/jquery.dynameter"+minified+".css"});

            jQueryLib.addFile({type: "js", url: "js/jquery-1.11.3"+minified+".js"});
            jQueryLib.addFile({type: "js", url: "js/jquery-ui"+minified+".js"});
            jQueryLib.addFile({type: "js", url: "js/jquery.mobile-1.4.5"+minified+".js"});
            jQueryLib.addFile({type: "js", url: "js/gauge"+minified+".js"});
        }
        CoreAtlx.ExternalLibs.jQueryLib = null;
        return jQueryLib;
    });
}

