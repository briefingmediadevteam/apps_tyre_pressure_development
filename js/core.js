/**
 * Created by Adrian on 18/04/2014.
 */

function LoadAtlx(){

    window.meadyster = {
        conf:{
            loadjQuery:true,
            loadExtJs:false,
            jumpURL:'',
            debug:true
        },
        utils:{
            _MS_PER_DAY:1000 * 60 * 60 * 24,
            dateDiffDays:function(date1, date2){
                var utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
                var utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());
                return Math.floor((utc2 - utc1) / this._MS_PER_DAY);
            },
            daysPassed:function(date){
                return this.dateDiffDays(date,new Date());
            },
            isDefined:function isDefined( v ){
                try{
                    v === 1; // Some statement to trigger a look-up for the v variable
                }catch( e ){
                    console.log( 'not declared' );
                    return false;
                }
                return (typeof v != 'undefined' && v != null);
            },
            defineOnce:function defineOnce( variable, scope, definition ){
                var ref = scope[variable];
                if( !this.isDefined( ref ) ){
                    scope[variable] = definition();
                }
                else{
                    console.log( 'already defined' )
                }
            },
            endEvent:function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                e.stopPropagation();
            },
            redirect:function( url ){
                if( url ){
                    window.document.location.href = url;
                }
            }
        }
    };

    var utils = meadyster.utils;

// define Atlx library
    utils.defineOnce( 'Atlx', meadyster, function(){
        function Atlx(){
            this.CoreClasses = {};
            this.Libs = {};
            this.Styles = {};
            this.ExternalLibs = {};
            this._ExternalLibs = [];
            var me = this;
            utils.defineOnce( 'coreLoader', me, function(){
                var AtlLoader = function(){
                    this.list = [];
                    this.loading = false;
                };
                AtlLoader.prototype = {
                    activeFile: null,
                    name: 'AtlLoader',
                    constructor: AtlLoader,
                    loading: false,
                    onComplete: null,
                    addEvents: function addEvents( elem ){
                        var loader = this;
                        if( elem.attachEvent ){
                            elem.attachEvent( "onreadystatechange", function(){
                                if( elem.readyState == "complete" || elem.readyState == "loaded" ){
                                    loader.loaded( elem );
                                }
                            } );
                        }
                        else if( elem.addEventListener ){
                            elem.addEventListener( "load", function(){
                                loader.loaded( elem );
                            }, false );
                        }
                    },
                    loadCSS: function loadCSS( obj ){
                        var script = document.createElement( "link" );
                        script.type = "text/css";
                        script.rel = "stylesheet";
                        script.href = obj.url;
                        this.addEvents( script );
                        document.getElementsByTagName( "head" )[0].insertBefore(
                            script,
                            document.getElementsByTagName( "title" )[0]
                        );
                    },
                    loadScript: function loadScript( obj ){
                        var script = document.createElement( "script" );
                        script.type = "text/javascript";
                        script.src = obj.url;
                        this.addEvents( script );
                        document.getElementsByTagName( "head" )[0].appendChild( script );
                    },
                    loaded: function loaded( elm ){
                        if( this.activeFile ){
                            console.log( 'loaded file:' + this.activeFile.url );
                            this.activeFile.loaded = true;
                            this.activeFile = null;
                        }
                        if( this.list.length > 0 ){
                            this.start();
                        }
                        else{
                            this.loading = false;
                            this.onComplete && this.onComplete( elm );
                        }
                    },
                    start: function start(){
                        var scr = this.list.shift();
                        if( utils.isDefined( scr ) ){
                            this.activeFile = scr;
                            if( scr.type == "css" ){
                                this.loadCSS( scr );
                            }
                            else if( scr.type == "js" ){
                                this.loadScript( scr );
                            }
                            else{
                                this.activeFile = null;
                                scr.loaded = true;
                                console.log( 'wierd type:' + scr.type );
                                this.loaded( null );
                            }
                        }
                        else{
                            this.loaded( null );
                        }
                    }
                };
                me.addLib( AtlLoader );
                me.CoreClasses.AtlLoader = AtlLoader;
                return new AtlLoader();
            } );
        }
        Atlx.prototype = {
            constructor: Atlx,
            initialized: false,
            isDefined: utils.isDefined,
            defineOnce: utils.defineOnce,
            Libs: null,
            Styles: null,
            ExternalLibs: null,
            _ExternalLibs: null,
            coreLoader: null,
            Utils: null,
            CoreClasses: null,
            //add a new LibObject library
            addLib: function addLib( lib ){
                this.Libs[lib.prototype.name] = lib;
            },
            //add a new LibObject style
            addStyle: function addStyle( lib ){
                this.Styles[lib.name] = lib;
            },
            //add a new LibObject external library (usualy including multiple files)
            addExternal: function addExternal( lib ){
                this.ExternalLibs[lib.name] = lib;
                this._ExternalLibs[lib.weight] = lib;
            },
            // load the external dependencies (libraries ) added with addExternal
            // complete -> callback for loading done
            loadExternal: function loadExternal( complete ){
                var l   = this._ExternalLibs.length,
                    lib = null, me = this;

                for( var i = 0; i < l; i++ ){
                    lib = this._ExternalLibs.shift();
                    if( lib && lib.load && !lib.loaded ){
                        lib.onLoaded = function(){
                            me.loadExternal.apply( me, [complete] );
                        };
                        console.log( 'start loading lib:' + lib.name );
                        lib.loadNow();
                        return;
                    }
                }
                console.log( 'done loading externals' );
                this.coreLoader.onComplete = complete;
                this.coreLoader.start();
            },
            // init Atlx
            // complete -> callback for loading done
            init: function( complete ){
                this.loadExternal( complete );
            },
            // helper redirect
            redirect: meadyster.utils.redirect,
            // general interface for running a function under a library (having some lib as scope of execution)
            runUnder: function( libName, func ){
                var lib = this.ExternalLibs[libName];
                if( this.isDefined( lib ) ){
                    lib.runUnder( func );
                }
            }
        };
        return new Atlx();
    } );

    window.CoreAtlx = meadyster.Atlx;

// define library object used to manage and store js and css files organised or not in libraries
    CoreAtlx.defineOnce( 'LibObject', CoreAtlx.CoreClasses, function(){
        var atlxLoader = CoreAtlx.coreLoader;

        function LibObject( name, weight, load ){
            this.name = name || 'lib';
            this.weight = weight || 0; // load order
            this.load = load || false; // load on app init
            this.files = [];
            this.que = [];
        }

        LibObject.prototype = {
            constructor: LibObject,
            load: false,// autoload
            que: null,// list of functions to call when the lib is done loading, registered with runUnder
            name: 'lib',// lib name
            weight: 0,// loading weight, allows changing the loading order of files
            files: null,// files that are part of this lib
            loaded: false,// is loaded ?
            isFileLoaded: null,
            // add a new file to the library
            addFile: function addFile( obj ){
                obj.loaded = false;
                this.files.push( obj );
            },
            // internal called when the lib files are done loading
            // calls onLoaded
            // calls all callbacks registered with runUnder
            _onLoaded: function(){
                console.log( 'library ' + this.name + ' loaded' );
                this.onLoaded();
                var func = null;
                while( this.que.length > 0 ){
                    func = this.que.shift();
                    this.runUnder( func );
                }
            },

            runUnder: function( f ){
            },

            onLoaded: function(){

            },
            // start loading the lib
            loadNow: function(){
                if( !this.load ){
                    return;
                }
                if( this.isLoaded() ){
                    return this._onLoaded();
                }
                var f, l = this.files.length, me = this;
                for( var i = 0; i < l; i++ ){
                    f = this.files[i];
                    if( !f.loaded ){
                        atlxLoader.list.push( f );
                    }
                }
                atlxLoader.onComplete = function(){
                    console.log( 'lib loaded' );
                    me._onLoaded.apply( me );
                };
                atlxLoader.start();
            }
        };
        CoreAtlx.CoreClasses.LibObject = null;
        return LibObject;
    } );

// special LibObject for dependencies that are not libraries (defined with LibObject)
// loads before everything else
    CoreAtlx.defineOnce( 'requires', CoreAtlx, function(){
        requires = new CoreAtlx.CoreClasses.LibObject( 'requires', 0, false );
        requires.beforeContinue = null;
        requires.alldone = false;
        requires.chain = true;
        requires.load = true;
        requires.isLoaded = function(){
            return requires.alldone;
        };
        requires.runUnder = function( func ){
            if( this.isLoaded() ){
                func();
            }
            else{
                this.que.push( func );
                console.log( 'required files not loaded' );
            }
        };

        requires._onLoaded = function(){
            requires.alldone = true;
            console.log( 'required files loaded as Atlx.requires ...' );
            requires.beforeContinue && requires.beforeContinue();
            CoreAtlx.CoreClasses.LibObject.prototype._onLoaded.apply( this );
        };

        CoreAtlx.addExternal( requires );
        CoreAtlx.requires = null;
        return requires;
    } );

// special LibObject for dependencies that are not libraries (defined with LibObject)
// loads after everything else
    CoreAtlx.defineOnce( 'loadLast', CoreAtlx, function(){
        loadLast = new CoreAtlx.CoreClasses.LibObject( 'loadLast', 20, false );
        loadLast.beforeContinue = null;
        loadLast.alldone = false;
        loadLast.load = true;
        loadLast.isLoaded = function(){
            return loadLast.alldone;
        };
        loadLast.runUnder = function( func ){
            if( this.isLoaded() ){
                func();
            }
            else{
                this.que.push( func );
                console.log( 'required files not loaded' );
            }
        };

        loadLast._onLoaded = function(){
            loadLast.alldone = true;
            console.log( 'loadLast files loaded as Atlx.loadLast ...' );
            loadLast.beforeContinue && loadLast.beforeContinue();
            CoreAtlx.CoreClasses.LibObject.prototype._onLoaded.apply( this );
        };

        CoreAtlx.addExternal( loadLast );
        CoreAtlx.loadLast = null;
        return loadLast;
    } );

}
