document.addEventListener( "deviceready", function(){

    LoadAtlx();
    CoreAtlx.requires.addFile( {
        type:"js", url:"js/atlx.jquery.js"
    } );
    CoreAtlx.loadLast.addFile( {
        type:"js", url:"js/sha512.js"
    } );
    CoreAtlx.loadLast.addFile( {
        type:"js", url:"js/md5.js"
    } );
    CoreAtlx.loadLast.addFile( {
        type:"js", url:"js/tyreapp.js"
    } );

    CoreAtlx.init( function(){

        if( CoreAtlx.initialized ){
            console.log( 'load complete already called' );
            return;
        }
        console.log( 'load complete called' );
        CoreAtlx.ExternalLibs.jQueryLib.runUnder( function(){

            j$( 'body' ).css( 'display', 'inherit' );
            j$( '#nav-panel' ).css( 'display', 'block' );
            TyreApp.scaleContent();

            document.addEventListener( "backbutton", function( e ){
                e.preventDefault();

                navigator.notification.confirm( "Are you sure you want to quit?", function( result ){
                    if( result == 2 ){
                        navigator.app.exitApp();
                    }
                }, 'Quit TyreApp', 'Cancel,Ok' );

            }, false );

            if( utils.isDefined( TyreApp.db.loaded ) && TyreApp.db.loaded )
            {
                j$('#login-wig' ).css('visibility','visible');
                TyreApp.setupLogin();
            }
        });
        CoreAtlx.initialized = true;
    } );

} );
